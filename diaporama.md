## Présentation des fonctionnalités avancées

---

<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

<div class="container">
<div class="col">
Charles Poulmaire

<img src="./castor_forge.svg" />
</div>
<div class="col">
Vincent-Xavier Jumel

<img src="./perso.jpg" />

</div>
</div>

---

[Modèle LaTeX](https://forge.aeif.fr/jn-rennes-2023/modele-latex)

![](./Latex.png)

----

[Annales bac NSI LaTeX](https://forge.aeif.fr/jn-rennes-2023/annales)

![](./annales.png)

---

[Annuaire](https://forge.aeif.fr/jn-rennes-2023/annuaire)

![](./annuaire.png)

----

[Rapport de
test](https://forge.aeif.fr/vincentxavier/annuaire/-/pipelines/7921/test_report)

![](./2_rapport_test.png)

----

[La requete de
fusion](https://forge.aeif.fr/vincentxavier/annuaire/-/merge_requests/3)

![](3_fusion.png)

---

[md-book](https://forge.aeif.fr/cpoulmaire/pdf)

![](md-book.png)
